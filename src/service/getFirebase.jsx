import firebase from "firebase"
import 'firebase/firestore'


const firebaseConfig = {
    apiKey: "AIzaSyD_9kj74iR6gQ3eka-_KSc4icZ1qO3FQ30",
    authDomain: "comision16155.firebaseapp.com",
    projectId: "comision16155",
    storageBucket: "comision16155.appspot.com",
    messagingSenderId: "337651131777",
    appId: "1:337651131777:web:67ed5d93ead66051423480"
};


const app = firebase.initializeApp(firebaseConfig)


// export function getFirebase(){
//     return app
// }

export function getFirestore(){
    
    return firebase.firestore(app)
}