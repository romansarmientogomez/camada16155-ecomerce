import { useState } from "react";
import Select from "./Select";



const options = [
    { value: 1, text: "Azul" },
    { value: 2, text: "Rojo" }
    
  ];

export default function Caso2() {

  const [option, setOption] = useState(1);

  function optionSelected(value) {
    setOption(value);
    console.log(value);
  }
  
  return (
    <>
        <Select
            options={options}
            onSelect={optionSelected} 
            defaultOption={option} />
    </>
  );
}
