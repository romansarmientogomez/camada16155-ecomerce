import {useState} from 'react'
import { Link } from 'react-router-dom'
 


const ItemCount = ({initial, stock, onAdd, cambiarBoton}) => {
    const [count, setCount] = useState(initial)


    const handlerAdd =()=>{
        setCount(count +1)        
    }

    const handlerRm =()=>{
        if(count > initial) setCount(count - 1)
    }   

    const handlerOnAdd=()=>{
        onAdd(count)
        setCount(initial)
        
    }

    return (
        <div className="w-50">
            <button className="btn btn-primary" onClick={handlerAdd}>+</button>
            <label>{count}</label>
            <button className="btn btn-primary" onClick={handlerRm}>-</button><br />      
            {
                !cambiarBoton && <button className="btn btn-outline-primary btn-block" onClick={handlerOnAdd}>Agregar</button>
            }   
              
        </div>           
    )
}


export default ItemCount
