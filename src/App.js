import {BrowserRouter as Router, Switch, Route} from 'react-router-dom'

import {useState, createContext } from "react";
import NavBar from './components/NabVar/NavBar';
import ItemDetailContainer from './components/containers/ItemDetailContainer/ItemDetailContainer';
import ItemListContainer from './components/containers/ItemListContainer/ItemListContainer';

import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css'
import Contacto from './components/Contacto/Contacto';
import AppContextProvider from './Context/AppContext';
import Cart from './components/Cart/Cart';



//https://pokeapi.co/api/v2/pokemon

//const productoObj = 

function App() { 

    //const [producto, setProducto] = useState(productoObj)



    return (
        < AppContextProvider>       
       
        <div 
            className="App border border-4 border-primary" 
            //onClick={handelClickApp} 
        >
            <Router>
                <NavBar />               
                    <Switch>
                        <Route exact path='/'>
                            <ItemListContainer titulo={'hola'} />
                        </Route>

                        <Route exact path='/categoria/:category'>
                            <ItemListContainer titulo={'hola'} />
                        </Route>

                        <Route exact path="/contacto">
                            <Contacto />
                        </Route>    

                        <Route exact path='/detalle' >
                            <ItemDetailContainer />
                        </Route>
                        <Route exact path='/cart'>
                            <Cart />
                        </Route>
                        
                    </Switch>
                {/* <Footer /> */}
                {/* <ItemCount /> */}
            </Router>
            </div>
            </AppContextProvider>
    );
}

export default App;
