import { useState, useEffect } from "react"
import {Spinner} from "react-bootstrap";

import { getProduc } from "../../../utils/promesas";
import ItemDetail from "./ItemDetail";

//codigo js

function ItemDetailContainer() {
    //codigo js
    const [ producto, setProducto ] = useState({})
    const [loading, setloading] = useState(true)

    useEffect(() => {
       getProduc
       .then(resp => {
           setProducto(resp)
           setloading(false)
       }) 
    }, [])


    return (
        <>
            {loading ? 
                    <Spinner animation="grow" variant="info" />
                : 
                <div>
                    <ItemDetail producto={producto}  />                   
                </div>
            }
            
        </>
    )
}

export default ItemDetailContainer
