//No se usa
//import React from 'react'
import { NavLink } from 'react-router-dom'

import Navbar from 'react-bootstrap/Navbar'
import Nav from 'react-bootstrap/Nav'
import Container from 'react-bootstrap/Container'
import { useAppContext } from '../../Context/AppContext'




function NavBar(){
    const remera= 'remera'

    const { iconCart } = useAppContext()

    console.log(iconCart())

    return( 
        <Navbar collapseOnSelect expand="lg" bg="dark" variant="dark">
            <Container>
                <NavLink to="/" style={{textDecoration: 'none'}}>
                    <Navbar.Brand to="/">React-Bootstrap</Navbar.Brand>                
                </NavLink>
            <Navbar.Toggle aria-controls="responsive-navbar-nav" />
            <Navbar.Collapse id="responsive-navbar-nav">
                <Nav className="me-auto">  
                    <Nav.Link className="ml-2" >
                        <NavLink to="/categoria/gorra" style={{textDecoration: 'none'}} className="text-secondary"  activeClassName="text-white">
                            Gorra                            
                        </NavLink>
                    </Nav.Link> 
                    <Nav.Link className="ml-2">
                        <NavLink to={`/categoria/${remera}`} style={{textDecoration: 'none'}} className="text-secondary"  activeClassName="text-white">
                                Remera                            
                        </NavLink>                
                    </Nav.Link> 
                    <Nav.Link className="ml-2">
                        <NavLink to={`/cart`} style={{textDecoration: 'none'}} className="text-secondary"  activeClassName="text-white">
                                { iconCart() } cart                            
                        </NavLink>                
                    </Nav.Link> 
                    
                </Nav>                
            </Navbar.Collapse>
            </Container>            
        </Navbar>
    )
}
export default NavBar